/*
 * This script takes a fresh import of jordstykke table and esrejerview view from the mssql ois database.
 * The geometry and information of the danish cadastrals in the ois-database are combined with
 * borehole geometry from the Jupiter database 
 * 
 * Author: Simon Makwarth <simak@mst.dk>
 * Licence: GNU Public licence version 3 (GPLv3)
 * Maintainer: Simon Makwarth <simak@mst.dk>
 * Repository: https://gitlab.com/mst-gko/pgois
 */

BEGIN;

	ALTER TABLE mst_ois.jordstykke
  		DROP COLUMN IF EXISTS geom
  	;
  
  	ALTER TABLE mst_ois.jordstykke
  		ADD COLUMN geom geometry(Polygon ,25832)
  	;
  
  	UPDATE mst_ois.jordstykke SET
		geom = st_geomfromtext(geom_wkt, 25832)
	;

	CREATE INDEX IF NOT EXISTS ois_geom_index
       ON mst_ois.jordstykke USING GIST (geom);
    
    CREATE INDEX IF NOT EXISTS esr_ejendomsnummer_idx
        ON mst_ois.jordstykke USING btree (esr_ejendomsnummer);
       
  	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mst_ois.jordstykke IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' | User: simak'
		     || ' | Descr: Table containing all jordstykker from the ois database' 
		     || ' | Repo: https://gitlab.com/mst-gko/pgois'
		     || '''';
		END
	$do$
	;

	-- MAT_ADR
	CREATE INDEX IF NOT EXISTS mat_adr_esr_esrejdnr_idx
        ON mst_ois.mat_adr USING btree (esrejdnr);
       
  	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mst_ois.mat_adr IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' | User: simak'
		     || ' | Descr: Table containing all cadastral adresses from the ois database' 
		     || ' | Repo: https://gitlab.com/mst-gko/pgois'
		     || '''';
		END
	$do$
	;

	-- combined jordstykke and mat_adr 
	DROP TABLE IF EXISTS mst_ois.jordstykke_mat_adr;

	CREATE TABLE mst_ois.jordstykke_mat_adr AS 
		(
			SELECT 
				j.*,
				vejkode AS mat_vejkode, 
				vejnavn AS mat_vejnavn, 
				hus_nr AS mat_hus_nr,
				supbynavn AS mat_supbynavn, 
				postnr AS mat_postnr, 
				postbynavn AS mat_postbynavn
			FROM mst_ois.jordstykke j 
			LEFT JOIN mst_ois.mat_adr ma ON j.esr_ejendomsnummer = (RIGHT(ma.komkode, 3) || '0' || ma.esrejdnr)::NUMERIC
		)
	;

	CREATE INDEX IF NOT EXISTS jordstykke_mat_ejer_esr_ejendomsnummer_idx
        ON mst_ois.jordstykke_mat_adr USING btree (esr_ejendomsnummer)
   	;
   
   	CREATE INDEX IF NOT EXISTS jordstykke_mat_ejer_geom_index
        ON mst_ois.jordstykke_mat_adr USING GIST (geom)
   	;
       
  	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mst_ois.jordstykke_mat_adr IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' | User: simak'
		     || ' | Descr: Table containing all cadastral geometries and adresses from the ois database' 
		     || ' | Repo: https://gitlab.com/mst-gko/pgois'
		     || '''';
		END
	$do$
	;

	-- drink_vv_bbr
	ALTER TABLE mst_ois.drink_vv_bbr
  		DROP COLUMN IF EXISTS geom
  	;
  
  	ALTER TABLE mst_ois.drink_vv_bbr
  		ADD COLUMN geom geometry(Polygon ,25832)
  	;
  
  	UPDATE mst_ois.drink_vv_bbr SET
		geom = st_geomfromtext(geom_wkt, 25832)
	;

	CREATE INDEX IF NOT EXISTS drink_vv_bbr_geom_index
        ON mst_ois.drink_vv_bbr USING GIST (geom)
   	;

   	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mst_ois.drink_vv_bbr IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' | User: simak'
		     || ' | Descr: Table containing all bbr information regarding drinkingwater plants' 
		     || ' | Repo: https://gitlab.com/mst-gko/pgois'
		     || '''';
		END
	$do$
	;
   
	-- public areas
	ALTER TABLE mst_ois.public_areas
  		DROP COLUMN IF EXISTS geom
  	;
  
  	ALTER TABLE mst_ois.public_areas
  		ADD COLUMN geom geometry(Polygon ,25832)
  	;
  
  	UPDATE mst_ois.public_areas SET
		geom = st_geomfromtext(geom_wkt, 25832)
	;

	CREATE INDEX IF NOT EXISTS public_areas_geom_index
        ON mst_ois.public_areas USING GIST (geom)
   	;
   
   	   
  	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mst_ois.public_areas IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' | User: simak'
		     || ' | Descr: Table containing all public areas' 
		     || ' | Repo: https://gitlab.com/mst-gko/pgois'
		     || '''';
		END
	$do$
	;

	-- set access to reader user
	GRANT USAGE ON SCHEMA mst_ois TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA mst_ois TO grukosreader;


COMMIT;
