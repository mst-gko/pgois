ALTER TABLE mst_ois.drink_vv_bbr 
	DROP COLUMN IF EXISTS geom
;

ALTER TABLE mst_ois.drink_vv_bbr 
	ADD COLUMN geom geometry
;
	
UPDATE mst_ois.drink_vv_bbr 
	SET geom = ST_GeomFromText(geom_wkt, 25832) 
;
	
CREATE INDEX drink_vv_bbr_geom_idx
	ON mst_ois.drink_vv_bbr 
	USING GIST (geom)
;