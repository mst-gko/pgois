SELECT 
	AdgAdr_id AS adgadr_id,
	KomKode AS komkode, 
	KomEjerlavKode AS komEjerlavKode, 
	LandsejerlavKode AS landsejerlavKode, 
	MatrNr AS matrnr,  
	VejKode AS vejkode, 
	VEJ_NAVN AS vejnavn, 
	SupBynavn AS supbynavn, 
	PostNr AS postnr, 
	PostByNavn AS postbynavn, 
	HUS_NR AS hus_nr,
	ESREjdNr AS esrejdnr
FROM dbo.CO42000T
;
