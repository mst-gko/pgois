WITH 
    grund AS 
        (
            SELECT DISTINCT
                KOMMUNE_NR, 
                KomKode, 
                ESREjdNr, 
                MatrNr, 
                VEJ_NAVN, 
                HusNr, 
                PostNr, 
                PostByNavn, 
                GRU_VandForsy_Kode_T
            FROM NyBBR_GrundView
            WHERE ObjStatus = 1
                AND GRU_VandForsy_Kode_T IS NOT NULL 
        )
SELECT 
    j.esr_Ejendomsnummer, j.matrikelnummer, j.geometri.STAsText() AS geom_wkt, 
    g.GRU_VandForsy_Kode_T, g.VEJ_NAVN, g.HusNr, g.PostNr, g.PostByNavn
FROM jordstykke j
INNER JOIN grund g ON j.esr_Ejendomsnummer = concat(g.KOMMUNE_NR, '0', g.ESREjdNr)
	AND j.matrikelnummer = g.MatrNr
;