SELECT
	j.featureID AS jordstykke_id,
	j.KommuneNr, 
	j.landsejerlavskode,
	j.esr_Ejendomsnummer,
	j.matrikelnummer,
	j.sfe_Registrering,
	j.l_NoteringsType,
	j.geometri.STAsText() AS geom_wkt
FROM OIS_CFK.dbo.Jordstykke j
;
