SELECT DISTINCT
	j.KommuneNr, 
	j.landsejerlavskode,
	j.esr_Ejendomsnummer,
	j.matrikelnummer,
	j.sfe_Registrering,
	j.l_NoteringsType,
	j.vejAreal, 
	ev.EJER_NAVN,
	EJER_CONAVN,
	ev.EJER_ADR,
	ev.EJER_UDV_ADR,
	ev.EJER_POSTADR, 
 	ev.EJERFORHOLD_KODE, 
 	EJERFORHOLD_KODE_T,
 	j.geometri.STAsText() AS geom_wkt
FROM OIS_CFK.dbo.ESREjerView ev 
INNER JOIN OIS_CFK.dbo.Jordstykke j ON j.esr_Ejendomsnummer = ev.KomEjdNr 
WHERE EJERFORHOLD_KODE IN (50, 60, 70, 80) 