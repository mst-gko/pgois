DROP SCHEMA IF EXISTS jupiter_fdw CASCADE;

CREATE SCHEMA IF NOT EXISTS jupiter_fdw;

CREATE EXTENSION IF NOT EXISTS postgres_fdw;

DROP SERVER IF EXISTS fdw_jupiter_ois CASCADE;

CREATE SERVER IF NOT EXISTS fdw_jupiter_ois FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '10.33.131.50', dbname 'pcjupiterxl', port '5432');

CREATE USER MAPPING IF NOT EXISTS FOR grukosadmin SERVER fdw_jupiter_ois OPTIONS (USER 'grukosadmin', password 'merkel30rblaser');

IMPORT FOREIGN SCHEMA jupiter FROM SERVER fdw_jupiter_ois INTO jupiter_fdw;