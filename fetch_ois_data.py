#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
This script is storing OIS information (cadastrals and ownership)
from a MSSQL server via pandas to a local MST-GKO PostGreSQL database.

License is GNU Public License version 3 (GPL v3)
https://www.gnu.org/licenses/gpl-3.0.html
"""
import time
import os

import pandas as pd
import configparser as cp
import psycopg2 as pg
from sqlalchemy import create_engine
import pymssql

__title__ = 'pgois'
__authors__ = 'Simon Makwarth <simak@mst.dk>'
__license__ = "GPLv3"
__maintainer__ = "Simon Makwarth <simak@mst.dk>"
__repository__ = f"https://gitlab.com/mst-gko/{__title__.lower().replace(' ', '_')}"
__issues__ = f'{__repository__}/-/issues'
__description__ = f'''{__title__} extracts cadastral and owner information from the MST-OD lifa MSSQL OIS database
and inserts into MST-GKO postgres OIS database.'''


class Database:
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.database_name = database_name

    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: db credentials
        """
        ini_database_name = self.database_name.upper()

        config = cp.ConfigParser()
        config.read(self.ini_file)

        usr = config[f'{ini_database_name}']['userid']
        pw = config[f'{ini_database_name}']['password']
        host = config[f'{ini_database_name}']['host']
        port = config[f'{ini_database_name}']['port']
        dbname = config[f'{ini_database_name}']['databasename']

        credentials = dict()
        credentials['userid'] = usr
        credentials['password'] = pw
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_mssql_db(self):
        cred = self.parse_db_credentials()
        database = cred['dbname']

        try:
            mssql_conn = pymssql.connect(cred['host'], cred['userid'], cred['password'], cred['dbname'])
            del cred
        except Exception as e:
            print(e)
            raise ValueError(f'Unable to connect to mssql database: {database}')

    def execute_pg_sql(self, sql):
        """
        connect to postgresql database and execute sql
        :param sql: string with sql query
        :return: None
        """

        cred = self.parse_db_credentials()
        pgdatabase = cred['dbname']
        try:
            with pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['userid'],
                password=cred['password']
            ) as con:
                with con.cursor() as cur:
                    cur.execute(sql)
                    con.commit()
        except Exception as e:
            print(f'Unable to execute sql: {pgdatabase}:')
            print(e)
        else:
            del cred
            print(f'Connected to database: {pgdatabase} and sql executed')

    def export_df_to_pg_db(self, df, schemaname, tablename):
        """
        Export pandas df to postgresDB
        :param df: pandas dataframe
        :param schemaname: database schema to export dataframe to
        :param tablename: database table to export dataframe to
        :return: None
        """
        cred = self.parse_db_credentials()
        pgdatabase = cred['dbname']

        try:
            df.columns = df.columns.str.lower()
            pg_conn = f"postgresql+psycopg2://{cred['userid']}:{cred['password']}@{cred['host']}:{cred['port']}/{cred['dbname']}"
            sql_engine = create_engine(pg_conn)#, use_batch_mode=True)
            del cred
            df.to_sql(tablename, con=sql_engine, schema=schemaname, if_exists='replace', index=False)
        except Exception as e:
            print('Table could not be edited: ' + pgdatabase + '.' + schemaname + '.' + tablename)
            print(e)
        else:
            print(f'dataframe is exported to postgresql database {pgdatabase}')

    def import_mssql_to_df(self, mssql_sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param mssql_sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """
        df_mssql = None
        mssql_conn, mssql_ois_dbname = self.connect_to_mssql_db()

        try:
            df_mssql = pd.read_sql(mssql_sql, mssql_conn)
            mssql_conn.close()
        except Exception as e:
            print(f'Unable to fetch dataframe from {mssql_ois_dbname}')
            print(e)
        else:
            print(f'Dataframe loaded from database: {mssql_ois_dbname}')
            print(df_mssql.head(5))

        return df_mssql

    @staticmethod
    def open_sql_file(sql_path):
        """
        opens sql file and store query as string
        :param sql_path: path to sql file
        :return: sql query as string
        """
        with open(sql_path, 'r') as fd:
            sql_file = fd.read()
        return sql_file


class Ois:
    def __init__(self):
        self.db_pg = Database(usr='writer', database_name='GRUKOS')
        self.db_mssql = Database(usr='writer', database_name='MSSQLOIS')
        self.cwd = os.path.dirname(os.path.abspath(__file__))

    def fetch_jordstykke_to_df(self):
        jordstykke_sql_path = f'{self.cwd}/sql/select_jordstykke.sql'
        sql_jordstykke = self.db_mssql.open_sql_file(jordstykke_sql_path)
        df_jordstykke = self.db_mssql.import_mssql_to_df(sql_jordstykke)

        return df_jordstykke

    def fetch_matadr_to_df(self):
        mat_adr_sql_path = f'{self.cwd}/sql/select_mat_adr.sql'
        sql_mat_adr = self.db_mssql.open_sql_file(mat_adr_sql_path)
        df_mat_adr = self.db_mssql.import_mssql_to_df(sql_mat_adr)

        return df_mat_adr

    def fetch_plant_adr(self):
        sql_path_bbr_vv = f'{self.cwd}/sql/select_bbr_drinkingwaterplants.sql'
        sql_bbr_vv = self.db_mssql.open_sql_file(sql_path_bbr_vv)
        df_bbr_vv = self.db_mssql.import_mssql_to_df(sql_bbr_vv)

        return df_bbr_vv

    def fetch_public_areas(self):
        sql_path_public_areas = f'{self.cwd}/sql/select_offentlige_arealer.sql'
        sql_public_areas = self.db_mssql.open_sql_file(sql_path_public_areas)
        df_public_areas = self.db_mssql.import_mssql_to_df(sql_public_areas)

        return df_public_areas

    def export_ois_to_pg_db(self, schemaname='mst_ois'):
        print('Importing mssql ois query to pandas dataframe...')
        df_jordstykke = self.fetch_jordstykke_to_df()
        df_mat_adr = self.fetch_matadr_to_df()
        df_bbr_vv = self.fetch_plant_adr()
        df_public_areas = self.fetch_public_areas()
        print('Pandas dataframes successfully imported.')

        print('\nExporting jordstykke to postgres ois db...')
        self.db_pg.export_df_to_pg_db(df=df_jordstykke, schemaname=schemaname, tablename='jordstykke')
        print('jordstykke successfully exported to postgres ois db.')

        print('\nExporting matadr to postgres ois db...')
        self.db_pg.export_df_to_pg_db(df=df_mat_adr, schemaname=schemaname, tablename='mat_adr')
        print('matadr successfully exported to postgres ois db.')

        print('\nExporting drinkingwater plant BBR to postgres ois db...')
        self.db_pg.export_df_to_pg_db(df=df_bbr_vv, schemaname=schemaname, tablename='drink_vv_bbr')
        print('drinkingwater plant BBR successfully exported to postgres ois db.')

        print('\nExporting public areas to postgres ois db...')
        self.db_pg.export_df_to_pg_db(df=df_public_areas, schemaname=schemaname, tablename='public_areas')
        print('Public areas successfully exported to postgres ois db.')

        print('\nCreating Jupiter foreign data wrapper for postgres ois database...')
        fdw_sql_path = f'{self.cwd}/sql/fdw_pcjupiter_to_ois.sql'
        sql_fdw = self.db_pg.open_sql_file(fdw_sql_path)
        self.db_pg.execute_pg_sql(sql_fdw)
        print('Jupiter foreign data wrapper for postgres ois database successfully created.')

        print('\nCreating geometry + index og jordstykke table and generate  ois database...')
        fetch_jordstykke_sql_path = f'{self.cwd}/sql/adm_borehole_ois.sql'
        sql_fetch_jordstykke = self.db_pg.open_sql_file(fetch_jordstykke_sql_path)
        self.db_pg.execute_pg_sql(sql_fetch_jordstykke)
        print('Index, geometry and table created.')


# start timer
t = time.time()

ois = Ois()
ois.export_ois_to_pg_db()

# end timer
elapsed = time.time() - t

print(f'ois_extract executed in {elapsed} s')
